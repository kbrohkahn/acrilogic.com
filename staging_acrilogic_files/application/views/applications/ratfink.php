<div class="subheader">An addicting trick-taking card game and the predecessor to Bridge</div>
<div class="badge-container">
  <a target="blank" href="https://play.google.com/store/apps/details?id=com.play2think.ratfink">
    <img alt="Get it on Google Play" src="/assets/img/en_generic_rgb_wo_45.png" />
  </a>
</div>
<h2>Description</h2>
<p class="indented">Ratfink is a trick taking card game and a fun and easy way learn the basics of the bridge card game!</p>
<p class="indented">Do you enjoy strategy, card games, or math? Do you want to learn card statistics or the basics of bridge such as bidding and trick taking? Even if you just want to play an enjoyable thought-oriented game, you should try Ratfink! Take our tutorial and you will know everything you need to know about Ratfink in less than 5 minutes!</p>
<h2>Features</h2>
<ul>
  <li>Control iTunes music app (iOS only)</li>
  <li>Play games with hands from as little as 1 card to as many as 13</li>
  <li>Play against 1, 2, or 3 robot opponents</li>
  <li>Select your game's difficulty</li>
  <li>Take a tutorial to learn the basics of bidding and trick taking in Ratfink</li>
  <li>Arrange the way the suits are ordered in your hand</li>
  <li>Customize the game to your liking</li>
</ul>

<h2>Screenshots</h2>
<div class="nav-tabs">
  <ul>
    <li class="active"><a href="#droid_maxx_screenshots">Droid Maxx</a></li>
    <li><a href="#galaxy_tab_2_7_screenshots">Galaxy Tab 2 7.0</a></li>
    <li><a href="#galaxy_tab_s_10_screenshots">Galaxy Tab S 10.1</a></li>
  </ul>
</div>
<div class="nav-tabs-content clearfix">
  <div id="droid_maxx_screenshots" style="display: block;">
    <div class="screenshot">
      <img src="/assets/img/music_home/droid_maxx/home.png" alt="Home screen">
      <div>
        <em>Home screen</em>
      </div>
    </div>
  </div>
</div>

<h2>Download Ratfink for free</h2>
<div class="badge-container">
  <a target="blank" href="https://play.google.com/store/apps/details?id=com.play2think.ratfink">
    <img alt="Get it on Google Play" src="/assets/img/en_generic_rgb_wo_45.png" />
  </a>
</div>

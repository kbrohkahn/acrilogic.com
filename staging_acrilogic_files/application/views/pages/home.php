<h2>Find our apps</h2>
<div class="badge-container">
  <a target="blank" class="apple-badge" href="http://appstore.com/acrilogic">
		<img alt="Find in App Store" src="/assets/img/Download_on_the_App_Store_Badge_US-UK_135x40.svg">
	</a>
	<a target="blank" href="https://play.google.com/store/search?q=pub:acrilogic">
		<img alt="Get it on Google Play" src="/assets/img/en_generic_rgb_wo_45.png" />
	</a>
</div>